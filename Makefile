###############################################################################
# DEFAULT & CUSTOM PATHS
###############################################################################
# Define GCC path
GCC_PATH = /usr/bin/gcc
# Define the cuda paths
CUDA_PATH = /usr/local/cuda-5.5/cuda
# Define the SDK path
CUDA_SDK = $(CUDA_PATH)/sdk
# Define any directories containing header files other than /usr/include
INCLUDES = -I$(CUDA_PATH)/include -I$(CUDA_SDK)/C/common/inc
# Define library paths in addition to /usr/lib using -Lpath
LFLAGS = -L$(CUDA_PATH)/lib64 -lcuda
# Define the project directory
PRJ_DIR = QR
# Define the doc, source and bin path
DOC = doc
BIN = bin
SRC = src

###############################################################################
# GCC and CUDA compilers
###############################################################################
# The compiler gcc for C program, defined as g++ for C++
CC = gcc
# The compiler nvcc for CUDA program
NVCC = nvcc

###############################################################################
# COMPILER & LINKER FLAGS
###############################################################################
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
#  -O3   m
CFLAGS  = -g -Wall -O3
# Take a look here docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
#  -m64  This option signals that the target platform is a 64-bit x86 platform
#  -ccbin Specify the directory in which the host compiler resides
NVCCFLAGS = -g --compiler-options -Wall -m64 -ccbin $(GCC_PATH)
# Define any libraries to link into executable using -llibname
#  -lm  adds the math library
LIBS = -lm

###############################################################################
# RULES
###############################################################################
# The build target executable
CPU_T = qr_cpu
GPU_T = qr_gpu
# Define the C source files
CPU_SRCS = $(SRC)/qr_cpu.c
GPU_SRCS = $(SRC)/qr_gpu.cu
# Define the C object files
CPU_OBJS = $(CPU_SRCS:.c=.o)
GPU_OBJS = $(GPU_SRCS:.cu=.o)
# Define the executable file 
MAIN = $(CPU_T) $(GPU_T)

all: $(MAIN)

$(CPU_T): $(CPU_OBJS)
	$(CC) $(CFLAGS) -o $(CPU_T) $(CPU_OBJS) $(LIBS)

# This is a suffix replacement rule for building .o's from .c's, it uses 
# automatic variables "$<" the name of the prerequisite of the rule(a .c file) 
# and "$@" the name of the target of the rule (a .o file). 
# (see the gnu make manual section about automatic variables)
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(GPU_T): $(GPU_OBJS)
	$(NVCC) $(INCLUDES) $(NVCCFLAGS) -o $(GPU_T) $(GPU_OBJS) $(LFLAGS) 
	
%.o: %.cu
	$(NVCC) $(INCLUDES) $(NVCCFLAGS) -c $< -o $@

install:
	@echo "Moving the binary file..."
	mv -v $(MAIN) $(BIN)
	
clean:
	@echo "Cleaning all..."
	$(RM) -v $(SRC)/*.o $(SRC)/*~ $(BIN)/$(MAIN)
